#!/usr/bin/env python3

try:
    from sys import argv, exit, stderr
    from termcolor import colored, cprint
    import sqlite3
    from datetime import datetime
    from pathlib import Path
    import re
    from hashlib import md5
    from shlex import split
    import logging
    from collections import defaultdict
    from configparser import ConfigParser
except ModuleNotFoundError as e:
    stderr.write(e)
    stderr.write(s)('Install appropriate version separately')
    exit(1)

QUERY_CREATE_TABLE_LIST = 'create table if not exists list (name text, id integer primary key autoincrement)'
QUERY_CREATE_TABLE_ITEM = 'create table if not exists item (name text, list integer, checked int, time int, hash text primary key)'
QUERY_VIEW_LISTS = 'select * from list'
QUERY_VIEW_ITEMS = 'select * from item'
QUERY_VIEW_LISTITEMS = 'select * from item where list=?'
QUERY_ADD_LIST = 'insert into list (name) values (?)'
QUERY_ADD_LIST_ID = 'insert into list (name,id) values (?,?)'
QUERY_ADD_ITEM = 'insert into item (name,list,checked,time,hash) values(?,?,?,?,?)'
QUERY_GET_LIST_FROM_ID = 'select * from list where id = ?'
QUERY_GET_LIST_FROM_NAME = 'select * from list where name = ?'
QUERY_GET_ITEM_FROM_HASH = 'select * from item where hash like ?'
QUERY_UPDATE_ITEM = 'update item set checked = ? where hash = ?'
QUERY_DELETE_LIST = 'delete from list where id = ?'
QUERY_DELETE_ITEM = 'delete from item where hash = ?'

_defaut_colors = {
    'id': 'magenta',
    'list': 'yellow',
    'hash':'cyan',
    'unchecked':'red',
    'checked':'green',
    'time':'yellow',
}

CONFIG = ConfigParser()
CONFIG.setdefault('color', _defaut_colors)
CONFIG.setdefault('log', {'level': 'DEBUG'})

SHODO_ROOT = Path.home()/'.shodo'
DB_FILE = '.shodo_db.db'
DB_PATH = str(SHODO_ROOT/DB_FILE)
LOG_FILE = 'shodo.log'
LOG_PATH = str(SHODO_ROOT/LOG_FILE)
CONFIG_FILE = 'config.ini'
CONFIG_PATH = str(SHODO_ROOT/CONFIG_FILE)

if not SHODO_ROOT.exists():
    SHODO_ROOT.mkdir()

def load_configs():
    CONFIG.read(CONFIG_PATH)

def write_config():
    _f = open(CONFIG_PATH, 'w')
    CONFIG.write(_f)
    _f.close()


load_configs()

logging.basicConfig(filename=LOG_PATH, filemode='a', format='[%(levelname)s] %(asctime)s | %(message)s', level=logging.getLevelName(CONFIG.get('log', 'level')))

DEFAULT_LIST_ID = 0
DEFAULT_LIST_NAME = 'default'

WIDTH = 25

_connection = sqlite3.connect(DB_PATH)
_cursor = _connection.cursor()


_cursor.execute(QUERY_CREATE_TABLE_LIST)
_cursor.execute(QUERY_CREATE_TABLE_ITEM)


def fix_len(s):
    CHARS = 20
    if len(s) > 20:
        return s[:17]+"..."
    elif len(s) < 20:
        return s.ljust(20)
    else:
        return s


class List:
    def __init__(this, _result):
        this.__name = _result[0]
        this.__id = _result[1]

    def __str__(this):
        return '['+colored(this.__id, CONFIG.get('color', 'id'))+']\t'+colored(this.__name, CONFIG.get('color', 'list'))

    def getName(this):
        return this.__name

    def getId(this):
        return this.__id

    def getItems(this):
        return list(map(Item, _cursor.execute(QUERY_VIEW_LISTITEMS, (this.__id,)).fetchall()))

    def delete(this):
        _cursor.execute(QUERY_DELETE_LIST, (this.__id,))

    @staticmethod
    def getAll():
        return list(map(List, _cursor.execute(QUERY_VIEW_LISTS).fetchall()))

    @staticmethod
    def getById(_id):
        _res = _cursor.execute(QUERY_GET_LIST_FROM_ID, (_id,)).fetchone()
        return List(_res) if _res is not None else None

    @staticmethod
    def getByName(_name):
        _res = _cursor.execute(QUERY_GET_LIST_FROM_NAME, (_name,)).fetchone()
        return List(_res) if _res is not None else None

    @staticmethod
    def add(_name, _id=None):
        if _id is None:
            _cursor.execute(QUERY_ADD_LIST, (_name,))
        else:
            _cursor.execute(QUERY_ADD_LIST_ID, (_name, _id))
        return _cursor.execute(QUERY_GET_LIST_FROM_NAME, (_name,)).fetchone()[1]


class Item:
    def __init__(this, _result):
        this.__name = _result[0]
        this.__list = List(_cursor.execute(
            QUERY_GET_LIST_FROM_ID, (_result[1],)).fetchone())
        this.__checked = _result[2]
        this.__time = _result[3]
        this.__hash = _result[4]

    def __str__(this):
        return f"\t{colored(colored('('+this.__hash+')', CONFIG.get('color', 'hash'), attrs=['underline']))}\t{colored(fix_len(this.__name), CONFIG.get('color', 'unchecked'), 'on_white', attrs=['bold']) if this.__checked==0 else colored(fix_len(this.__name), CONFIG.get('color', 'checked'))}\t{colored('['+datetime.fromtimestamp(this.__time).strftime('%A %d %B %Y %H:%M:%S')+']', CONFIG.get('color', 'time'))}"

    def getName(this):
        return this.__name

    def getList(this):
        return this.__list

    def check(this):
        this.__checked = 1

    def uncheck(this):
        this.__checked = 0

    def save(this):
        _cursor.execute(QUERY_UPDATE_ITEM, (this.__checked, this.__hash,))

    def delete(this):
        _cursor.execute(QUERY_DELETE_ITEM, (this.__hash,))

    @staticmethod
    def getByHash(_hash):
        _res = _cursor.execute(QUERY_GET_ITEM_FROM_HASH,
                               (_hash+'%',)).fetchone()
        return Item(_res) if _res is not None else None

    @staticmethod
    def getAllByHash(_hash):
        return list(map(Item, _cursor.execute(QUERY_GET_ITEM_FROM_HASH, (_hash+'%',)).fetchall()))

    @staticmethod
    def add(_name, _list=0, _checked=0):
        _ts = datetime.now()
        _hash = create_hash(_ts.__str__()+_name)
        _cursor.execute(QUERY_ADD_ITEM, (_name, _list,
                        _checked, int(_ts.timestamp()), _hash,))


def create_hash(_s):
    return md5((_s).encode()).hexdigest()


def _view(*args):
    """view|vw                              Show all items with list\nview|vw <list id>                    Show items in list <list>\nview|vw <item hash>                  Show full <item> name"""
    if len(args) == 0:
        _lists = List.getAll()
        if _lists:
            for _list in _lists:
                print(_list)
                for _item in _list.getItems():
                    print(_item)
        else:
            cprint('No lists', 'yellow')
    elif len(args) == 1:  # list/item_hash
        if args[0].isnumeric() and 0 < len(args[0]) < 3:
            _list = List.getById(args[0])
        else:
            _list = List.getByName(args[0])

        if _list is not None:
            print(_list)
            _items = _list.getItems()
            if _items:
                for _item in _items:
                    print(_item)
            else:
                cprint('List empty', 'yellow')
        else:
            _item = Item.getByHash(args[0])
            if _item is not None:
                print(_item.getName())


def _list():
    """list|ls                              Show all lists"""
    _lists = List.getAll()
    if _lists:
        for _list in _lists:
            print(_list)
    else:
        cprint('No lists', 'yellow')


def _add(*args):
    """add <item name>                      Add <item name> in default list\nadd <item name> <list id>            Add item <item name> to list <list id>"""
    if len(args) == 1:  # item
        if List.getById(DEFAULT_LIST_ID) is None:
            List.add(DEFAULT_LIST_NAME, DEFAULT_LIST_ID)

        Item.add(args[0])
        cprint('Item added', 'green')

        _view(str(DEFAULT_LIST_ID))

    elif len(args) == 2:  # item list
        if 0 < len(args[1]) < 3 and args[1].isnumeric():
            _list = List.getById(args[1])
        else:
            _list = List.getByName(args[1])

        if _list is None:
            _list_id = List.add(args[1])
        else:
            _list_id = _list.getId()

        Item.add(args[0], _list_id)
        cprint('Item added', 'green')

        if _list is not None:
            _view(str(_list.getId()))

    else:
        print("Usage:")
        cprint(_add.__doc__, 'red')


def _create(_name):
    """create <list name>                   Create empty list"""
    _cursor.execute(QUERY_ADD_LIST, (_name,))
    cprint('List added', 'green')


def _remove(*args):
    """remove|rm <list id/item hash>,[...]  Remove item matching <item hash> or list <list id>"""
    if 0 < len(args[0]) < 3:
        _list = List.getById(args[0])
        if _list:
            try:
                if (input(f'Remove list "{_list.getName()}" and its contents ({len(_list.getItems())} items) ? [Y/n] ').lower() == 'y'):
                    for _item in _list.getItems():
                        _item.delete()
                    _list.delete()
                    cprint('Removed list: ['+args[0]+']', 'yellow')
                else:
                    raise KeyboardInterrupt()
            except KeyboardInterrupt:
                print()
                cprint('Cancelled!', 'yellow')
        else:
            cprint('Nothing found matching '+args[0], 'red')
    else:
        _items = Item.getAllByHash(args[0])
        if len(_items) == 1:
            try:
                if (input('Remove item "'+_items[0].getName()+'" ? [Y/n]').lower() == 'y'):
                    _items[0].delete()
                    cprint('Removed item: "'+_items[0].getName()+'"', 'yellow')
                else:
                    raise KeyboardInterrupt()
            except KeyboardInterrupt:
                print()
                cprint('Cancelled!', 'yellow')
        elif len(_items) > 1:
            cprint('Ambigous hash, use longer', 'yellow')
        else:
            cprint('No item matching hash '+args[0], 'red')


def _check(_hash):
    """check|chk <item hash>                Mark item <item> as checked"""
    _items = Item.getAllByHash(_hash)
    if len(_items) == 1:
        _items[0].check()
        _items[0].save()
        _view(str(_items[0].getList().getId()))
    elif len(_items) > 1:
        cprint('Ambigous hash, use longer', 'yellow')
    else:
        cprint('No matching item found', 'yellow')


def _uncheck(_hash):
    """uncheck|uck <item hash>              Mark item matching <item hash> as unchecked"""
    _items = Item.getAllByHash(_hash)
    if len(_items) == 1:
        _items[0].uncheck()
        _items[0].save()
        _view(str(_items[0].getList().getId()))
    elif len(_items) > 1:
        cprint('Ambigous hash, use longer', 'yellow')
    else:
        cprint('No matching item found', 'yellow')


def _clean():
    """clean|cln                            Delete empty lists"""
    _lists = List.getAll()
    if _lists:
        for _list in _lists:
            if len(_list.getItems()) == 0:
                _list.delete()
    else:
        cprint('No lists', 'yellow')


def _help():
    """help                                 Show help message"""
    print("shodo                                Start in command mode")
    print("shodo <cmd> [...args]", "\n")
    print(_view.__doc__)
    print(_list.__doc__)
    print(_add.__doc__)
    print(_create.__doc__)
    print(_remove.__doc__)
    print(_check.__doc__)
    print(_uncheck.__doc__)
    print(_clean.__doc__)
    print(_help.__doc__)


def shodo(_cmd, _args):
    _argc = len(_args)
    logging.info("CMD %s %s", _cmd, _args)

    if _argc > 2:
        cprint('Too many arguments', 'red')
        exit()

    if _cmd in ('view', 'vw'):
        if _argc == 0:  # view
            _view()
        elif _argc == 1:  # view list
            _view(_args[0])
        else:
            print("Usage:")
            cprint(_view.__doc__, 'red')
    elif _cmd in ('list', 'ls'):
        if _argc == 0:  # list
            _list()
        else:
            print("Usage:")
            cprint(_list.__doc__, 'red')
    elif _cmd in ('add', "+"):
        if _argc == 1:  # add item
            _add(_args[0])
        elif _argc == 2:  # add item list
            _add(_args[0], _args[1])
        else:
            print("Usage:")
            cprint(_add.__doc__, 'red')
    elif _cmd in ('create'):
        if _argc == 1:  # create name
            _create(_args[0])
        else:
            print("Usage:")
            cprint(_create.__doc__, 'red')
    elif _cmd in ('remove', 'rm', '-'):
        print(_cmd, _argc, _args)
        if _argc > 0:  # remove list
            for _arg in _args.split(','):
                _remove(_args[0])
        else:
            print("Usage:")
            cprint(_remove.__doc__, 'red')
    elif _cmd in ('check', 'chk'):
        if _argc == 1:  # check item
            _check(_args[0])
        else:
            print("Usage:")
            cprint(_check.__doc__, 'red')
    elif _cmd in ('uncheck', 'uchk', 'uck'):
        if _argc == 1:  # uncheck item
            _uncheck(_args[0])
        else:
            print("Usage:")
            cprint(_check.__doc__, 'red')
    elif _cmd in ('clean', 'cln'):
        if _argc == 0:  # clean
            _clean()
        else:
            print("Usage:")
            cprint(_view.__doc__, 'red')
    elif _cmd in ('help', '--help'):
        _help()


def main():
    _exec, _cmd, _args = None, None, None

    if len(argv) == 1:
        try:
            _cmd, *_args = split(input("shodo > "))
            shodo(_cmd, _args)
            if _cmd not in ('quit', 'exit', ':q'):
                main()
        except (KeyboardInterrupt, EOFError):
            cprint('Exit', 'yellow')
    else:
        _exec, _cmd, *_args = argv
        shodo(_cmd, _args)


if __name__ == '__main__':
    try:
        main()
    finally:
        _connection.commit()
        _connection.close()
        write_config()
